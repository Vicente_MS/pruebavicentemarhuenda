<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
	public function scopeLatest($query){
		return $query->orderBy("id","desc");
	}

	public function scopePrice($query){
		return $query->orderBy("pricing","desc");
	}

	public function scopeTitle($query){
		return $query->orderBy("title","desc");
	}

}

