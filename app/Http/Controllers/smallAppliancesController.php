<?php

/*

 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Product;

use Illuminate\Support\Facades\Auth;




class smallAppliancesController extends Controller
{
	public function categories(){

		$products = Product::latest()->simplePaginate(35);
		return view('smallAppliances',["products" => $products]);


	}

}
