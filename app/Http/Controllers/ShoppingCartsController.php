<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\ShoppingCart;
use App\InShoppingCart;

use Illuminate\Support\Facades\Auth;

class ShoppingCartsController extends Controller
{

	public function __construct(){
		$this->middleware("shoppingcart");
	}

    public function checkout(){
        return view('/');
    }

    public function index(Request $request){

    	$shopping_cart = $request->shopping_cart;

        $in_shopping_cart = $shopping_cart->inShoppingCarts()->get();
    	

        $products = $shopping_cart->products()->get();

        $total = $shopping_cart->total();

        return view("shopping_carts.index", ["products" => $products,"total" => $total, "in_shopping_cart" => $in_shopping_cart]);
    }


     public function destroy($id)
    {
        InShoppingCart::destroy($id);

        echo "$id";
         return redirect('/carrito');
    }
}
