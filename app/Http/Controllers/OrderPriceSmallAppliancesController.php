<?php

/*

 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Product;




class OrderPriceSmallAppliancesController extends Controller
{

	public function price(){

		$products = Product::Price()->simplePaginate(15);

		return view('main.smallAppliancesprice',["products" => $products]);

	}


	

}