<?php

/*

 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Product;




class MainController extends Controller
{
	public function home(){

		$products = Product::latest()->simplePaginate(15);

		return view('main.home',["products" => $products]);

	}

	public function price(){

		$products = Product::Price()->simplePaginate(15);

		return view('main.home',["products" => $products]);

	}

	public function title(){

		$products = Product::Title()->simplePaginate(15);

		return view('main.home',["products" => $products]);

	}
	



}