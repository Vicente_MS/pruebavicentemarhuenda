<div class="card product text-left" style="width: 100%;
	max-width: 100%;
	padding: 1em;
	position: relative;
	margin:10px auto;">

		@if(Auth::check() && $product->user_id == Auth::user()->id)

		<div class="absolute actions">
			<a href="{{url('/products/'.$product->id.'/edit')}}">Edit</a>

			@include('products.delete',['product' => $product])
			
		</div>

		@endif

		<h1>{{$product->title}}</h1>

		<div class="row">
			<div class="col-sm-6 col-xs-12">
				@if($product->extension)
					<img src="{{url("/products/images/$product->id.$product->extension")}}" class="product-avatar" style="max-width: 100%; margin-top:25px;">

				@endif
			</div>




			<div class="col-sm-6 col-xs-12" style="text-align: right; padding-right: 100px; margin-top:25px;">
				<p style="text-align: left;">
					{{$product->category}}
				</p>
				<p style="text-align: left;">
					<strong>Description: </strong>
				</p>
				<p style="text-align: left;">
					{{$product->description}}
				</p>

				<p style="color: red;">
					Price:   {{$product->pricing}}€
				</p>

				<p>
					@include("in_shopping_carts.form",["product" => $product])
				</p>
			</div>



		</div>
	</div>