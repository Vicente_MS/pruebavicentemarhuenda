		{!! Form::open(['url' => $url, 'method' => $method, 'files' => true])  !!}

		<div class="form-group">
			{{ Form::text('title',$product->title,['class' => 'form-control', 'placeholder' =>'Title...'])  }}
		</div>


		<div class="form-group">
			{{ Form::textarea('description',$product->description,['class' => 'form-control', 'placeholder' => 'describe...']) }}
		</div>


		<div class="form-group">
			{{ Form::number('pricing',$product->pricing,['class' => 'form-control', 'placeholder' => 'Price...']) }}
		</div>

	 	<div class="form-group">
			 {{ Form::text('category',$product->category,['placeholder' => 'category']) }} 

		</div> 



		<div class="form-group">
			{{ Form::file('cover') }}
		</div>


		<div class="form-group text-right">
			<a href="{{url('/products')}}"> return to the list of products </a>
			<input type="submit" name="" value="Send" class="btn btn-success">
		</div>

		{!! Form::close() !!}