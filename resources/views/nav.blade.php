



<nav class="navbar navbar-expand-lg navbar-light bg-light" style="font-size: 1.4em; padding-left: 70px; padding-bottom: 20px;">
  <a class="navbar-brand" href="{{ url('/') }}" style="font-size: 1.4em;">HOME</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" style="margin-right: 45px;">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="smallAppliances">SMALL APPLIANCES<span class="sr-only"></span></a>
      </li>
       <li class="nav-item active">
        <a class="nav-link" href="dishwasher">DISHWASHER<span class="sr-only"></span></a>
      </li>
    </ul>
  </div>
</nav>