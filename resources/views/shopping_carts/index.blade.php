@extends("layouts.app")

@section("content")

<div class="big-padding text-center blue-grey white-text" style="color: white; background-color: teal;">
	<h1>BASKET</h1>
</div>

<div class="container">
	<table class="table table-bordered">
		<thead>
			<tr>
				<td>Image</td>
				<td>Product</td>
				<td>Price</td>
				<td>Accion</td>
			</tr>
		</thead>

		<tbody>
			<?php
			$variablenew = 0;
			?> 

			@foreach($products as $product)
			<tr>
				<td>			
					<div class="col-sm-6 col-xs-12">
						@if($product->extension)
						<img src="{{url("/products/images/$product->id.$product->extension")}}" class="product-avatar" style="max-width: 100%; margin-top:25px;">
{{$product->category}}
						@endif

					</div></td>
				<td>{{$product->title}}</td>
				<td>{{$product->pricing}}</td>

				<?php 						
				$in_shopping_cartr = $in_shopping_cart ->get($variablenew);
				$variablenew ++;

				  ?> 

				<td>@include('shopping_carts.ShopDelete', ['in_shopping_cartr' => $in_shopping_cartr])</td> 
				
			</tr>
			@endforeach

			<tr>
				<td>Total</td>
				<td>{{$total}}</td>
			</tr>

		</tbody>
		
	</table>
	<div class="text-right">
		@include("shopping_carts.form")
	</div>
</div>

@endsection

