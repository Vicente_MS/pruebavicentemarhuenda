
@extends('layouts.app')

@section('Title','Products')

@section('content')
@include("orderSmallAppliances")
	<div class="" style="width: 80%; height: 100%; margin: auto;">
		<div class="container text-center products-container">

			@foreach($products as $product)

			<?php
			if ($product ['category'] == 'SMALL APPLIANCES')
			{?>
				@include("products.product",["product" => $product])
				<?php
			}
			?>


			@endforeach

			<div style="margin-left: 40%;">
				{{$products->links()}}
			</div>
		</div>
	</div>
	
@endsection