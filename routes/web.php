<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@home');



Route::get('/carrito', 'ShoppingCartsController@index');
Route::post('/carrito', 'ShoppingCartsController@checkout');



Route::get('/title', 'OrderTitleController@title');
Route::get('/price', 'OrderPriceController@price');

Route::get('/smallAppliancestitle', 'OrderTitleSmallAppliancesController@title');
Route::get('/smallAppliancesprice', 'OrderPriceSmallAppliancesController@price');

Route::get('/dishwashertitle', 'OrderTitleDishwasherController@title');
Route::get('/dishwasherprice', 'OrderPriceDishwasherController@price');

Route::get('/smallAppliances', 'smallAppliancesController@categories');

Route::get('/dishwasher', 'dishwasherController@categorie');

Auth::routes();

Route::resource('carrito','ShoppingCartsController');

Route::resource('products','ProductsController');


Route::resource('in_shopping_carts','InShoppingCartsController',[
'only' => ['store', 'destroy']
]);

Route::get('products/images/{filename}',function($filename){
	$path = storage_path("app/images/$filename");

	if (!\File::exists($path)) abort(404); 
		$file = \File::get($path);

		$type = \File::mimeType($path);

		$response = Response::make($file,200);

		$response->header("Content-Type", $type);

		return $response;
	
});


/*

GET /products => index
POST /products => store
GET /products/create => formulario para crear

GET /products/:id => mostrar un producto con id
GET /products/:id/edit 
PUT/PATCH /products/:id
DELETE /products/:id

*/

Route::get('/home', 'HomeController@index')->name('home');
